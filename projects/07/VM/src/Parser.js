const operations = require("./Operations");

function map(register) {
  switch (register) {
    case "argument":
      return "ARG";
    case "constant":
      return "constant";
    case "local":
      return "LCL";
    case "temp":
      return "";
    case "that":
      return "THAT";
    case "this":
      return "THIS";
  }
}

class Parser {
  preParse(file) {
    return file
      .split("\n")
      .map(line =>
        line.indexOf("//") > -1
          ? line.substr(0, line.indexOf("//")).trim()
          : line.trim()
      )
      .filter(str => str.length);
  }

  routeCommandType(line, counter) {
    const command = line.split(" ")[0];
    const arg1 = line.split(" ")[1];
    const arg2 = line.split(" ")[2];

    switch (command) {
      case "label":
        return this.handleLabel(line.split(" ")[1]);
      case "push":
        return this.handlePush(arg1, arg2);
      case "pop":
        return this.handlePop(arg1, arg2);
      case "add":
        return operations.ADD;
      case "sub":
        return operations.SUB;
      case "neg":
        return operations.NEG;
      case "and":
        return operations.AND;
      case "or":
        return operations.OR;
      case "not":
        return operations.NOT;
      case "eq":
        return operations.returnEQ(counter);
      case "gt":
        return operations.returnGT(counter);
      case "lt":
        return operations.returnLT(counter);
      default:
        return Error(`Invalid command: ${command}, at line: ${line}`);
    }
  }

  handlePop(register, num) {
    var commands = `// POP_${register}_${num}
    ${operations.DECREMENT_SP}\n`;

    switch (register) {
      case "temp":
        commands += `A=M
          D=M
          @${Number(num) + 5}
          M=D
          \n\n
        `;
        break;
      case "pointer":
        var pointer = num == 0 ? (pointer = "THIS") : (pointer = "THAT");
        commands += `A=M
          D=M
          @${pointer}
          M=D\n\n`;
        break;

      case "static":
        commands += `A=M
        D=M
        @${Number(num) + 16}
        M=D
        \n\n`;
        break;

      default:
        // local, argument, this, that
        commands += `@${map(register)}
        D=M
        @${num}
        D=D+A
        @R13
        M=D
        ${operations.GET_REGISTER_VAL("SP")}
        @R13
        A=M
        M=D\n\n
      `;
        break;
    }

    return commands;
  }

  handleLabel(label) {
    return "handle label: " + label;
  }

  handleCall(data) {
    return "handleCall" + data;
  }

  handleFunction(data) {
    return "handleFunction" + data;
  }

  handleReturn(data) {
    return "handleReturn" + data;
  }

  handleGoto(data) {
    return "handle Goto" + data;
  }

  handleIfGoTo(data) {
    return "handle ifGoTo" + data;
  }

  handlePush(register, value) {
    var commands = `// PUSH_${register}_${value}\n`;

    switch (register) {
      case "constant":
        commands += `${operations.SET_D_TO(value)}`;
        break;
      case "static":
        commands += `@${Number(value) + 16}
        D=M\n`;
        break;
      case "local":
        commands += `${operations.STORE_REGISTER_VALUE_IN_TEMP("LCL")}
        ${operations.SET_D_TO(value)}
        @R13
        M=D+M
        A=M
        D=M
        ${operations.SET_STACK_TO_D}
        \n`;
        break;
      case "temp": //ERROR
        commands += `@${Number(value) + 5}
        D=M
        ${operations.SET_STACK_TO_D}\n`;
        break;
      case "pointer":
        var location = "";
        value == 0 ? (location = "THIS") : (location = "THAT");
        commands += `@${location}
        D=M
        ${operations.SET_STACK_TO_D}\n`;
        break;
      default:
        // TODO: for argument, this, that
        commands += `${operations.STORE_REGISTER_VALUE_IN_TEMP(map(register))}
          ${operations.SET_D_TO(value)}
          @R13
          D=D+M
          M=D
          A=M
          D=M\n`;
        break;
    }

    commands += `${operations.SET_STACK_TO_D}
      ${operations.INCREMENT_SP}\n
    `;
    return commands;
  }

  parse(fileContents) {
    const lines = this.preParse(fileContents);

    let asmCode = operations.INIT;

    lines.forEach((data, i) => {
      asmCode += this.routeCommandType(data, i);
    });

    return asmCode.replace(/ +?/g, "");
  }
}

module.exports = Parser;

// var symbolTable = {
//   SCREEN: 16384,
//   KBD: 24576,
//   SP: 0,
//   LCL: 1,
//   ARG: 2,
//   THIS: 3,
//   THAT: 4
// };

// set RAM[0] 256,
// set RAM[1] 300,
// set RAM[2] 400,
// set RAM[3] 3000,
// set RAM[4] 3010,

// "local n" refers to RAM[RAM[1]+n].
