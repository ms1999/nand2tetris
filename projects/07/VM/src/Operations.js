exports.INIT = `//INIT
@256
D=A
@SP
M=D
@300
D=A
@LCL
M=D
@400
D=A
@ARG
M=D
@3000
D=A
@THIS
M=D
@3010
D=A
@THAT
M=D\n\n`;

exports.DECREMENT_SP = `@SP
M=M-1`;

exports.INCREMENT_SP = `@SP
M=M+1`;

exports.SET_STACK_TO_D = `@SP
A=M
M=D`;

exports.GET_REGISTER_VAL = register => {
  return `@${register}
    A=M
    D=M\n`;
};

exports.SET_D_TO = val => {
  return `@${val}
    D=A\n`;
};

exports.STORE_REGISTER_VALUE_IN_TEMP = register => {
  return `@${register}
  D=M
  @R13
  M=D`;
};

exports.ADD = `//ADD
    @SP
    M=M-1
    A=M
    D=M
    @SP
    M=M-1
    A=M
    M=M+D
    @SP
    M=M+1\n\n`;

exports.SUB = `//SUB
    @SP
    M=M-1
    A=M
    D=M
    @SP
    M=M-1
    A=M
    M=M-D
    @SP
    M=M+1\n\n`;

exports.NEG = `//NEG
  @SP
  M=M-1
  A=M
  D=M
  M=0
  D=M-D
  M=D
  @SP
  M=M+1\n\n`;

exports.AND = `@SP
  M=M-1
  A=M
  D=M
  A=A-1
  D=D&M
  M=D\n\n`;

exports.OR = `@SP
  M=M-1
  A=M
  D=M
  A=A-1
  D=D|M
  M=D\n\n`;

exports.NOT = `@SP
  M=M-1
  A=M
  D=M
  D=!D
  M=D
  @SP
  M=M+1\n\n`;

exports.returnEQ = counter => `@SP
    M=M-1
    A=M
    D=M
    @SP
    M=M-1
    A=M
    D=D-M
    @NOT_EQUAL${counter}
    D;JNE
    //(EQUAL)
      @SP
      A=M
      M=-1
      @INC_POINTER${counter}
      0;JMP
    (NOT_EQUAL${counter})
      @SP
      A=M
      M=0
    (INC_POINTER${counter})
      @SP
      M=M+1\n\n`;

exports.returnLT = counter => `@SP
  M=M-1
  A=M
  D=M
  @SP
  M=M-1
  A=M
  D=D-M
  @NOT_LESS_THAN${counter}
  D;JLE
  //(LESS_THAN)
    @SP
    A=M
    M=-1
    @INC_POINTER${counter}
    0;JMP
  (NOT_LESS_THAN${counter})
    @SP
    A=M
    M=0
  (INC_POINTER${counter})
    @SP
    M=M+1\n\n`;

exports.returnGT = counter => `@SP
  M=M-1
  A=M
  D=M
  @SP
  M=M-1
  A=M
  D=D-M
  @GREATER_THAN${counter}
  D;JGE
  //(NOT_GREATER_THAN)
    @SP
    A=M
    M=-1
    @INC_POINTER${counter}
    0;JMP
  (GREATER_THAN${counter})
    @SP
    A=M
    M=0
  (INC_POINTER${counter})
    @SP
    M=M+1\n\n`;
