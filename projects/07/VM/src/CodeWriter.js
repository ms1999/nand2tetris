class CodeWriter {
  setFileName(fileName) {}
  writeInit() {}
  writeLabel(label) {}
  writeGoTo(label) {}
  writeIf(label) {}
  writeFunction(functionName, numVars) {}
  writeCall(functionName, numArgs) {}
  writeReturn() {}
}

module.exports = CodeWriter;
