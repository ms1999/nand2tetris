const fs = require("fs");
const path = require("path");
const outdent = require("outdent");

const Parser = require("./Parser");
const CodeWriter = require("./CodeWriter");

const args = process.argv.slice(2)[0];

class Main {
  constructor() {
    this.parser = new Parser();
    this.codeWriter = new CodeWriter();
  }

  returnInputArray(pathString) {
    const isDir = fs.lstatSync(pathString).isDirectory();

    if (!pathString.length) {
      console.log("Please pass in a file or directory");
      return [];
    } else if (isDir) {
      const directoryContents = fs.readdirSync(pathString);

      return directoryContents.map(fileName => {
        const fullPath = `${pathString}/${fileName}`;
        const rawFile = fs.readFileSync(fullPath).toString();
        const processedFile = this.parser.parse(rawFile).toString();

        return {
          contents: processedFile,
          fullPath
        };
      });
    } else {
      const rawFile = fs.readFileSync(pathString).toString();
      const processedFile = this.parser.parse(rawFile).toString();
      return [{ contents: processedFile, fullPath: pathString }];
    }
  }

  writeFile(fileName, contents) {
    fs.writeFileSync(fileName, contents);
  }

  init(input) {
    const fileArray = this.returnInputArray(input);
    fileArray.forEach(({ fullPath, contents }) => {
      console.log("fil", fullPath);
      this.writeFile(fullPath.replace(".vm", ".asm"), contents);
    });
  }
}

let main = new Main();
main.init(args);
