//INIT
@256
D=A
@SP
M=D
@300
D=A
@LCL
M=D
@400
D=A
@ARG
M=D
@3000
D=A
@THIS
M=D
@3010
D=A
@THAT
M=D

//PUSH_constant_10
@10
D=A
@SP
A=M
M=D
@SP
M=M+1

//POP_local_0
@SP
M=M-1
@LCL
D=M
@0
D=D+A
@R13
M=D
@SP
A=M
D=M

@R13
A=M
M=D


//PUSH_constant_21
@21
D=A
@SP
A=M
M=D
@SP
M=M+1

//PUSH_constant_22
@22
D=A
@SP
A=M
M=D
@SP
M=M+1

//POP_argument_2
@SP
M=M-1
@ARG
D=M
@2
D=D+A
@R13
M=D
@SP
A=M
D=M

@R13
A=M
M=D


//POP_argument_1
@SP
M=M-1
@ARG
D=M
@1
D=D+A
@R13
M=D
@SP
A=M
D=M

@R13
A=M
M=D


//PUSH_constant_36
@36
D=A
@SP
A=M
M=D
@SP
M=M+1

//POP_this_6
@SP
M=M-1
@THIS
D=M
@6
D=D+A
@R13
M=D
@SP
A=M
D=M

@R13
A=M
M=D


//PUSH_constant_42
@42
D=A
@SP
A=M
M=D
@SP
M=M+1

//PUSH_constant_45
@45
D=A
@SP
A=M
M=D
@SP
M=M+1

//POP_that_5
@SP
M=M-1
@THAT
D=M
@5
D=D+A
@R13
M=D
@SP
A=M
D=M

@R13
A=M
M=D


//POP_that_2
@SP
M=M-1
@THAT
D=M
@2
D=D+A
@R13
M=D
@SP
A=M
D=M

@R13
A=M
M=D


//PUSH_constant_510
@510
D=A
@SP
A=M
M=D
@SP
M=M+1

//POP_temp_6
@SP
M=M-1
A=M
D=M
@11
M=D



//PUSH_local_0
@LCL
D=M
@R13
M=D
@0
D=A

@R13
M=D+M
A=M
D=M
@SP
A=M
M=D

@SP
A=M
M=D
@SP
M=M+1

//PUSH_that_5
@THAT
D=M
@R13
M=D
@5
D=A

@R13
D=D+M
M=D
A=M
D=M
@SP
A=M
M=D
@SP
M=M+1

//ADD
@SP
M=M-1
A=M
D=M
@SP
M=M-1
A=M
M=M+D
@SP
M=M+1

//PUSH_argument_1
@ARG
D=M
@R13
M=D
@1
D=A

@R13
D=D+M
M=D
A=M
D=M
@SP
A=M
M=D
@SP
M=M+1

//SUB
@SP
M=M-1
A=M
D=M
@SP
M=M-1
A=M
M=M-D
@SP
M=M+1

//PUSH_this_6
@THIS
D=M
@R13
M=D
@6
D=A

@R13
D=D+M
M=D
A=M
D=M
@SP
A=M
M=D
@SP
M=M+1

//PUSH_this_6
@THIS
D=M
@R13
M=D
@6
D=A

@R13
D=D+M
M=D
A=M
D=M
@SP
A=M
M=D
@SP
M=M+1

//ADD
@SP
M=M-1
A=M
D=M
@SP
M=M-1
A=M
M=M+D
@SP
M=M+1

//SUB
@SP
M=M-1
A=M
D=M
@SP
M=M-1
A=M
M=M-D
@SP
M=M+1

//PUSH_temp_6
@11
D=M
@SP
A=M
M=D
@SP
A=M
M=D
@SP
M=M+1

//ADD
@SP
M=M-1
A=M
D=M
@SP
M=M-1
A=M
M=M+D
@SP
M=M+1

