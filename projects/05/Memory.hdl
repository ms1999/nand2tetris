// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/05/Memory.hdl

/**
 * The complete address space of the Hack computer's memory,
 * including RAM and memory-mapped I/O. 
 * The chip facilitates read and write operations, as follows:
 *     Read:  out(t) = Memory[address(t)](t)
 *     Write: if load(t-1) then Memory[address(t-1)](t) = in(t-1)
 * In words: the chip always outputs the value stored at the memory 
 * location specified by address. If load==1, the in value is loaded 
 * into the memory location specified by address. This value becomes 
 * available through the out output from the next time step onward.
 * Address space rules:
 * Only the upper 16K+8K+1 words of the Memory chip are used. 
 * Access to address>0x6000 is invalid. Access to any address in 
 * the range 0x4000-0x5FFF results in accessing the screen memory 
 * map. Access to address 0x6000 results in accessing the keyboard 
 * memory map. The behavior in these addresses is described in the 
 * Screen and Keyboard chip specifications given in the book.
 */

// For the HACK computer, memory addresses
// 0..16K-1	read and write from the RAM16K chip,
// 16K..24K-1	read and write from Screen chip addresses 0..8K-1, respectively,
// 24K	reads from the Keyboard chip.
// 24K+1..
// 64K-1	undefined what will happen (I.e., "don't try this at home, kids!")

//Figure 5.7 shows that the RAM is addresses 0-16383, the Screen is 16384-24575, and the Keyboard is 24576. 

CHIP Memory {
    IN in[16], load, address[15];
    OUT out[16];

    PARTS:
    // Put your code here:
    DMux(in=load, sel=address[14], a=loada, b=loadb);
    RAM16K(in=in, load=loada, address=address[0..13], out=ramOut);
    Screen(in=in, load=loadb, address=address[0..12], out=screenOut);
    Keyboard(out=keyOut);
    Mux4Way16(a=ramOut, b=ramOut, c=screenOut, d=keyOut, sel=address[13..14], out=out);
}



// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/03/b/RAM16K.hdl

// /**
//  * Memory of 16K registers, each 16 bit-wide. Out holds the value
//  * stored at the memory location specified by address. If load==1, then 
//  * the in value is loaded into the memory location specified by address 
//  * (the loaded value will be emitted to out from the next time step onward).
//  */

// CHIP RAM16K {
//     IN in[16], load, address[14];
//     OUT out[16];

//     PARTS:
//     // Put your code here:
//     DMux4Way(in=load, sel=address[0..1], a=loada, b=loadb, c=loadc, d=loadd);

//     RAM4K(in=in, load=loada, address=address[2..13], out=outa);
//     RAM4K(in=in, load=loadb, address=address[2..13], out=outb);
//     RAM4K(in=in, load=loadc, address=address[2..13], out=outc);
//     RAM4K(in=in, load=loadd, address=address[2..13], out=outd);

//     Mux4Way16(a=outa, b=outb, c=outc, d=outd, sel=address[0..1], out=out);
// }


// CHIP RAM64 {
//     IN in[16], load, address[6];
//     OUT out[16];

//     PARTS:
//     // Put your code here:
//     DMux8Way(in=load, sel=address[0..2], a=loada, b=loadb, c=loadc, d=loadd, e=loade, f=loadf, g=loadg, h=loadh);

//     RAM8(in=in, load=loada, address=address[3..5], out=outa);
//     RAM8(in=in, load=loadb, address=address[3..5], out=outb);
//     RAM8(in=in, load=loadc, address=address[3..5], out=outc);
//     RAM8(in=in, load=loadd, address=address[3..5], out=outd);

//     RAM8(in=in, load=loade, address=address[3..5], out=oute);
//     RAM8(in=in, load=loadf, address=address[3..5], out=outf);
//     RAM8(in=in, load=loadg, address=address[3..5], out=outg);
//     RAM8(in=in, load=loadh, address=address[3..5], out=outh);

//     Mux8Way16(a=outa, b=outb, c=outc, d=outd, e=oute, f=outf, g=outg, h=outh, sel=address[0..2], out=out);
// }