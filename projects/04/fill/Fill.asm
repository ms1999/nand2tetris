// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

(LOOP)

  @KBD
  D=M
  @CLEAR
    D;JLE
  @FILL
    D;JGT

  (FILL)
    @16384
    D=A 
    @addr 
    M=D

    (LOOP1)
      @addr // set current addr to all 1's
      A=M
      M=-1

      @addr //increment addr
      M=M+1

      @addr
      D=M
      @24576
      D=D-A 
      @LOOP
        D;JGE
      @LOOP1
        D;JLE
        
      
  (CLEAR)
    @16384
    D=A 
    @addr 
    M=D

    (LOOP2)
      @addr // set current addr to all 0's
      A=M
      M=0

      @addr //increment addr
      M=M+1

      @addr
      D=M
      @24576
      D=D-A 
      @LOOP
        D;JGE
      @LOOP2
        D;JLE
        

@LOOP
  0;JMP


// loop: 
//         i=KBD 
//         if i=0: 
//                 goTo NOTPRESSED 
//         else: 
//                 goTo PRESSED 

// PRESSED: 
//         a=16384 
//         loop1: 
//                 RAM[a]=1111111111111111 
//                 a++ 
//                 if a>24575: 
//                         goTo loop 
//                 else: 
//                         goTo 

// NOTPRESSED: 
//         a=16384 
//         loop2: 
//                 RAM[a]=0000000000000000 
//                 a++ 
//                 if a>24575: 
//                         goTo loop 
//                 else: 
//                         goTo loop2

